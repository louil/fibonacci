.intel_syntax noprefix

ERROR_MSSG:
			.asciz	"Correct command format:\n.\/fibonacci (positive number 0-300)\n"

.fmt:
		.string "Number: 0x%lx%lx%lx%lx\n"

.globl main
main:
	cmp		rdi, 2				#See if there are two red two arguments
	jne		ERROR				#Jump to error

	xor		eax, eax			#0 out Accumulator
	mov		rdi, [rsi + 8]		#Get the second value
	xor		rsi, rsi			#0 out Second Arg
	mov		rdx, 10				#Set 3rd arg to 10
	call	strtol				#Strtol(char * rdi, char ** rsi, int rdx)
								#strtol(argv[1], 0, 10)
								#Give long value of argv[1] in rax
	mov		rdi, rax			#Set rdi to rax
	cmp		[rsi], byte ptr 0	#Captures the return from strol when it finished
	jne		ERROR				#jump to error if not equal
	cmp		rdi, 300			#Check to see if return was above 300
	jg		ERROR				#Jump to error section
	cmp		rdi, 0				#Check to see if rdi is 0
	jl		ERROR				#Jump to error if less than

	#Mass register idea derived from paul iracane
	mov		R10, 1				#Set R10 to 1
	xor		R11, R11			#Set R10 to 0
	xor		R12, R12			#Set R10 to 0
	xor		R13, R13			#Set R10 to 0
	xor		R14, R14			#Set R10 to 0
	xor		R15, R15			#Set R10 to 0
	xor		rbx, rbx			#Set rbx to 0
	xor		rbp, rbp			#Set rbp to 0

Fibonacci:
	add		R10, R11			#R10 += R11
	adc		R12, R13			#R12 += R13 + cf
	adc		R14, R15			#R14 += R15 + cf
	adc		rbx, rbp			#rbx += rbp + cf

	#xchg usage derived from dsprimm
	xchg	R11, R10			#R11 and R10 swap what is stored between themselves
	xchg	R13, R12			#R13 and R12 swap what is stored between themselves
	xchg	R15, R14			#R15 and R14 swap what is stored between themselves
	xchg	rbp, rbx			#rbp and rbx swap what is stored between themselves

	sub		rdi, 1				#Rdi -= 1
	cmp		rdi, 0				#Check to see if rdi is 0
	jle 	Finish				#Jump to finish if rdi is less than or equal to 0
	jmp		Fibonacci			#Jump to Fibonacci to continue loop

Finish:
	mov		rdi, OFFSET .fmt	#Move print statement into rdi
    mov		rsi, rbp			#Set rsi to rbp
    mov		rdx, R15			#Set rdx to R15
	mov		rcx, R13			#Set rcx to R13
	mov		R8, R11				#Set R8 to R11
	xor		eax, eax			#Set eax to 0
	call	printf				#Call printf with its arguments
	ret							#Exit

ERROR:
	mov		rdi, OFFSET ERROR_MSSG	#Move the error message into rdi
	call	puts					#Call puts
	ret								#Exit
